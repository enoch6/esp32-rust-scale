# Overview

This is my first project where I'm attempting to use Rust to build an ESP32 project.  The project will ultimately 
calculate and serial console (maybe someday screen) a weight using a Wheatbridge resistor technique.

Lots of props to https://www.youtube.com/watch?v=vUSHaogHs1s for the base project and figuring out the many things 
I didn't want to focus on first.

## Device Info

| Name            | Item                                      | Link                                                                                                                                             |
|-----------------|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| Device          | esp32 Dev Module                          | [Amazon Link](https://a.co/d/7geAtWq)                                                                                                            |
| Chip            | SP32-D0WDQ6                               | [Datasheet PDF](https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf)                                              |
| Architecture    | Xtensa                                    |                                                                                                                                                  |
| Instruction Set | Xtensa                                    | [Instruction Set PDF](https://www.cadence.com/content/dam/cadence-www/global/en_US/documents/tools/silicon-solutions/compute-ip/isa-summary.pdf) |                                
| Processor       | Xtensa® 32-bit LX6 Dual Microprocessors   | ...                                                                                                                                              |
| Flash ROM       | 448 KB                                    | ...                                                                                                                                              |
| SRAM            | 520 KB                                    | ...                                                                                                                                              |
| SRAM in RTC     | 16 KB                                     | ...                                                                                                                                              |


## Rust 

* [Rust Platform Support Page](https://forge.rust-lang.org/platform-support.html) - NOTE: esp32 is NOT officially in the list of targets, need to build the toolchain 
* [Prereqs for LLVM Rust w/ esp32](https://github.com/esp-rs/esp-idf-template?tab=readme-ov-file#prerequisites)
* 

## Build Steps
1. `sudo chmod a+rw /dev/ttyUSB0`
2. `cargo +esp build`
3. `espflash flash target/xtensa-esp32-espidf/debug/output`

