/*
 Arduino pin 5 -> HX711 CLK
 Arduino pin 4 -> HX711 DOUT
 Arduino pin 5V -> HX711 VCC
 Arduino pin GND -> HX711 GND 
*/

#include "HX711.h"

#define LOADCELL_DOUT_PIN  4
#define LOADCELL_SCK_PIN  5
#define abs(x) ((x)>0?(x):-(x))

HX711 scale;

float units;
float ounces;
float calibration_factor = -7050; // initial starting guess

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 calibration sketch");
  Serial.println("Remove all weight from scale");
  Serial.println("After readings begin, place known weight on scale");
  Serial.println("Press + or a to increase calibration factor");
  Serial.println("Press - or z to decrease calibration factor");

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(calibration_factor); //
  scale.tare();	//Assuming there is no weight on the scale at start up, reset the scale to 0

  long zero_factor = scale.read_average(); //Get a baseline reading
  Serial.print("Zero factor: "); //This can be used to remove the need to tare the scale. Useful in permanent scale projects.
  Serial.println(zero_factor);

  Serial.println("Readings:");
}

void loop() {

  Serial.print("Reading: ");
  Serial.print(abs(scale.get_units()), 1); //scale.get_units() returns a float
  Serial.print(" lbs"); //You can change this to kg but you'll need to refactor the calibration_factor
  Serial.println();

  if(Serial.available())
  {
    char temp = Serial.read();
    if(temp == '+' || temp == 'a') {
      Serial.print("Increasing calibration factory by 10. Calibration factor is: ");
      calibration_factor += 10;
      Serial.println(calibration_factor);
    }
    else if(temp == '-' || temp == 'z') {
      Serial.print("Decreasing calibration factory by 10. Calibration factor is: ");
      calibration_factor -= 10;
      Serial.println(calibration_factor);
    }
  }

  delay(500); // read every 0.5 seconds
}